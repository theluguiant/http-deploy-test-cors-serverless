'use strict';

module.exports.hello = async (event, context, callback) => {
  console.log("context: ",context);
  console.log("event: ",event);
  const response = {
    statusCode: 200,
    statusMessage: "Success",
    body: {
        message: 'Hello world!'
      },
  };
  return response;
  
};
